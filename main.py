"""
Copydown: by MBM GROUP, 2015-2019, All rights reserved
"""

import re
import sys
import time
import requests
import xmltodict
import xml
import json
import time


def logi(text):
    text = str(text)
    print(text)
    with open("info.m", "a") as file:
        file.write(text+"\n")

# def secured(operation_name="unnamed action"):
#     def decorator(f):
#         try:
#             f()
#         except requests.exceptions.ConnectTimeout:
#             pass#logi("ConnectTimout error from {0}".format(operation_name))
#         except requests.exceptions.ReadTimeout:
#             pass#logi("ReadTimout error from {0}".format(operation_name))
#         except requests.exceptions.Timeout:
#             pass#logi("Timout error from {0}".format(operation_name))
#         except requests.exceptions.ProxyError:
#             pass#logi("Proxy error from {0}".format(operation_name))
#         except requests.exceptions.ConnectionError:
#             pass#logi("Connection error from {0}".format(operation_name))
#         except KeyError:
#             pass#logi("Key error from {0}".format(operation_name))
#         except TypeError:
#             pass#logi("Type error from {0}".format(operation_name))
#         except AttributeError:
#             pass#logi("Attribute error from {0}".format(operation_name))
#         # except Exception:
#         #     pass#logi("The Error from {0}".format(operation_name))
#         except xml.parsers.expat.ExpatError:
#             logi("THATS ZTE from {0}".format(operation_name))
#         return f
#     return decorator

 
class Huawei:
    data = {}
    template_ussd = "<?xml version='1.0' encoding='utf-8'?><request><content>{0}</content>" \
                   "<codeType>CodeType</codeType><timeout></timeout></request>"
    shortcuts = {
        "hardware_info": "device/basic_information",
        "notifications": "monitoring/check-notifications",
        "signal": "device/signal",
        "i": "device/information",
    }
    user_agent = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1)"
                      "AppleWebKit/537.36 (KHTML, like Gecko)"
                      "Chrome/39.0.2171.95 Safari/537.36"
    }
    imsi_of_operators = {
        "01": "mts",
        "02": "megafon",
        "11": "yota",
        "14": "megafon",
        "20": "tele2",
        "28": "beeline",
        "62": "tinkoff",
        "99": "beeline"
    }
    actions = {
        "beeline": {
            "number": "*110*10#",
            "balance": "*102#"
        },
        "megafon": {
            "number": "*205#",
            "balance": "*100#"
        },
        "mts": {
            "number": "*111*0887#",
            "balance": "*100#"
        },
        "yota": {
            "number": "*103#",
            "balance": "*100#"
        },
        "tele2": {
            "number": "*201#",
            "balance": "*105#"
        }
    }

    def __init__(self, module, port, face="http://192.168.8.1", timeout=15):
        self.default_timeout = 15        
        self.proxy = {
            "http": "http://{m}:{p}".format(p=port, m=module),
            "https": "http://{m}:{p}".format(p=port, m=module)
        }
        self.session = requests.Session()
        self.call_addr = "%s/api/" % face
        self.session.get(face, proxies=self.proxy, headers=self.user_agent, timeout=self.default_timeout*3)

    def __getitem__(self, key):
        if key in self.data.keys():
            return self.data[key]

    def token(self):
        tokenraw = self.raw_api("webserver/SesTokInfo")
        tokenml = xmltodict.parse(tokenraw, dict_constructor=dict, encoding="utf-8")
        token = tokenml["response"]["TokInfo"]
        self.header = self.user_agent
        self.header.update({
            "__RequestVerificationToken": token
        })
        self.data["token"] = token
        return token

    def raw_api(self, path):
        Get = self.session.get(self.call_addr + path, proxies=self.proxy, headers=self.user_agent, timeout=self.default_timeout)
        result = Get.text
        return result

    def sms_api(self, count=1):
        smsreq = "<?xml version='1.0' encoding='utf-8'?><request><PageIndex>1</PageIndex><ReadCount>{0}</ReadCount><BoxType>1</BoxType><SortType>0</SortType><Ascending>0</Ascending><UnreadPreferred>0</UnreadPreferred></request>".format(count)
        h = {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
             '__RequestVerificationToken': self.token()}
        Get = self.session.post(self.call_addr + "sms/sms-list",
                                proxies=self.proxy, headers=h,
                                data=smsreq, timeout=self.default_timeout,
                                stream=True)
        res = Get.content.decode("utf-8", "ignore").replace("\n", " ")
        try:
            res = xmltodict.parse(res, dict_constructor=dict, encoding="utf-8")
        except Exception:
            res = Get.text
        return res

    def hw(self):
        massive = self.raw_api(self.shortcuts["i"])
        response = xmltodict.parse(massive, dict_constructor=dict, encoding="utf-8")["response"]
        self.data.update(response)
        return self.data

    def send_ussd(self, command):
        Post = self.session.post(self.call_addr + "ussd/send", self.template_ussd.format(command),
                                 proxies=self.proxy, headers=self.header, timeout=self.default_timeout)
        result = Post.text
        return result

    def check_ussd(self):
        try:
            Get = self.session.get(self.call_addr + "ussd/get", proxies=self.proxy, timeout=self.default_timeout)
            result = Get.content.decode("utf-8", "ignore").replace("\n", " ")
            if "error" in result:
                return True
            else:
                return result
        except Exception:
            return False

    def operator(self):
        imsi = self["Imsi"]
        opCode = imsi[3:5]
        if opCode in self.imsi_of_operators.keys():
            return self.imsi_of_operators[opCode]
        else:
            print("OMG!", imsi)

    def ussd(self, command):
        self.token()
        self.send_ussd(command)
        answer = False
        breacker = 0
        while not type(answer) == str:
            time.sleep(5)
            answer = self.check_ussd()
            if type(answer) == str:
                answer = xmltodict.parse(answer, dict_constructor=dict, encoding="utf-8")['response']['content']
                return answer
            elif answer:
                break
            breacker += 1
            if breacker > 4: break
        return "fail"

    def send_message(self, phone, text):
        # curl -X POST -d "<request><Index>-1</Index><Phones><Phone>$phone</Phone></Phones><Sca></Sca><Content>$message</Content><Length>140</Length><Reserved>1</Reserved><Date>1</Date></request>" "http://$MODEM_IP/api/sms/send-sms" -H "Cookie: $SESSION_ID" -H "__RequestVerificationToken: $TOKEN"
        pass

    def last_sms(self):
        sms = self.sms_api(count=5)
        result = ""
        for s in sms['response']['Messages']['Message']:
            date = s['Date']
            text = s['Content']
            result += "{}\n".format(text)
        return result

    def balance(self):
        my_operator = self.operator()
        return self.ussd(self.actions[my_operator]["balance"])

    def number(self):
        my_operator = self.operator()
        return self.ussd(self.actions[my_operator]["number"])


import argparse

parser = argparse.ArgumentParser(description='Ussd to modem')
parser.add_argument('--ip', help='192.168.0.1 or 192.168.8.1')
parser.add_argument('--proxy', help='proxy ip or host (like modem1 or ip)')
parser.add_argument('--proxyport', help='1601 or 53301')
parser.add_argument('--ussd', help='ussd command (*100#)')
aargs = dict(parser.parse_args()._get_kwargs())

host = aargs.get("ip")
proxy = aargs.get("proxy")
proxyport = aargs.get("proxyport")
command = aargs.get("ussd")

medem = Huawei(module=proxy, port=proxyport, face=host)
print(medem.ussd(command))
